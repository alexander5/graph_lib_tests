from graph_tool.all import Graph, seed_rng, load_graph
from numpy.random import randint
from random import seed
import time
import defopt
 

def build(*, nnodes : int = 10, nedges : int = 4):

    t0 = time.time()

    g = Graph(directed=False)
    nodes = [g.add_vertex() for i in range(nnodes)]

    seed_rng(42)
    seed(123)

    # attach edges to nodes
    sources = randint(0, nnodes - 1, nedges)
    targets = randint(0, nnodes - 1, nedges)
    tic = time.time()
    g.add_edge_list(((sources[i], targets[i]) for i in range(nedges)))
    toc = time.time()
    print(f'attaching edges: {toc - tic:.2f} [s]')

    print(f'{nnodes} nodes:')
    # for i in g.iter_vertices():
    #     print(f'node {i}')

    print(f'{nedges} edges:')
    # print(g.get_edges())

    # save
    tic = time.time()
    g.save("simple.xml.gz")
    toc = time.time()
    print(f'save: {toc - tic:.2f} [s]')

    # load
    tic = time.time()
    g2 = load_graph("simple.xml.gz")
    toc = time.time()
    print(f'load: {toc - tic:.2f} [s]')

    # clean
    tic = time.time()
    del g2
    del g
    toc = time.time()
    print(f'clean: {toc - tic:.2f} [s]')

    t1 = time.time()
    print(f'total: {t1 - t0:.2f} [s]')
    print('-'*40)


if __name__ == '__main__':
    defopt.run(build)
