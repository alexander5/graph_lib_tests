data = {
    'contagion': {
        1000000: {'add_edges': 9.85, 'save': 12.11, 'load': 7.56, 'clean': 1.21, 'total': 31.92},
        2000000: {'add_edges': 22.54, 'save': 25.27, 'load': 15.62, 'clean': 3.90, 'total': 69.23},
        5000000: {'add_edges': 66.97, 'save': 70.24, 'load': 73.97, 'clean': 24.40, 'total': 240.69},
        10000000: {'add_edges': 133.07, 'save': 204.07, 'load': 756.64, 'clean': 198.88, 'total': 1303.42},
    },
    'networkx-pletzer': {
        1000000: {'add_edges': 8.24, 'save': 9.90, 'load': 7.20, 'clean': 0.47, 'total': 26.71},
        2000000: {'add_edges': 18.73, 'save': 20.27, 'load': 15.14, 'clean': 1.03, 'total': 18.73},
        5000000: {'add_edges': 58.23, 'save': 51.85, 'load': 44.34, 'clean': 3.10, 'total': 161.97},
        10000000: {'add_edges': 126.17, 'save': 148.54, 'load': 317.23, 'clean': 23.17, 'total': 624.30},
    },
    'graph-tool': {
        1000000: {'add_edges': 3.02, 'save': 3.33, 'load': 5.70, 'clean': 0.75, 'total': 13.60},
        2000000: {'add_edges': 6.17, 'save': 6.68, 'load': 11.64, 'clean': 1.66, 'total': 27.56},
        5000000: {'add_edges': 16.33, 'save': 16.99, 'load': 29.70, 'clean': 4.64, 'total': 71.47},
        10000000: {'add_edges': 35.11, 'save': 33.81, 'load': 60.57, 'clean': 10.27, 'total': 147.43},
    },
}

from matplotlib import pylab

c = ['r', 'b', 'g']
sym = {
    'add_edges': '*',
    'save': ':',
    'load': '--',
    'clean': '-.',
    'total': '-',
}
count = 0
legs = []
for lib, data_lib in data.items():

    color = c[count]

    x = data_lib.keys()

    count2 = 0
    for code_section in data_lib[1000000]:

        y = [data[lib][k][code_section] for k in x]
        pylab.plot(x, y, color + sym[code_section])
        legs.append(f'{lib} {code_section}')

        count2 += 1

    count += 1

pylab.legend(legs)
pylab.show()