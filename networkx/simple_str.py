import networkx
from numpy.random import randint
from random import seed
import time
import defopt
 
"""
Saves nodes and edges as string keys
"""

def build(*, nnodes : int = 10, nedges : int = 4):

    g = networkx.Graph()
    nodes = [g.add_node(str(i)) for i in range(nnodes)]

    seed(123)

    # attach edges to nodes
    sources = randint(0, nnodes - 1, nedges)
    targets = randint(0, nnodes - 1, nedges)
    tic = time.time()
    g.add_edges_from([(str(sources[i]), str(targets[i])) for i in range(nedges)])
    toc = time.time()
    print(f'adding edges: {toc - tic:.2f} [s]')

    print(f'{nnodes} nodes:')
    print(f'{nedges} edges:')

    #for mth in "adjlist", "edgelist", "gexf", "gml", "gpickle", "graphml", "pajek":
    #for mth in "adjlist", "edgelist", "gpickle":
    for mth in "gpickle",:
    	tic = time.time()
    	eval(f'networkx.write_{mth}(g, "simple.{mth}")')
    	toc = time.time()
    	print(f'{mth} save: {toc - tic:.2f} [s]')
    	tic = time.time()
    	g2 = eval(f'networkx.read_{mth}("simple.{mth}")')
    	toc = time.time()
    	print(f'{mth} load: {toc - tic:.2f} [s]')


if __name__ == '__main__':
    defopt.run(build)
